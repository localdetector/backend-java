FROM openjdk:9-b130-jre

WORKDIR data
COPY target/mySpring-1.0-SNAPSHOT.jar .

ENTRYPOINT ["java","-jar", "mySpring-1.0-SNAPSHOT.jar"]
