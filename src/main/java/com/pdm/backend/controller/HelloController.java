package com.pdm.backend.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: baojing.he
 * @Date: 2021-01-12 15:05
 * @Description:
 */

@Slf4j
@RestController
public class HelloController {


    @GetMapping(value = "/hello")
    public String hello() {
        log.info("hello world");
        return "hello world successfully";
    }
}
