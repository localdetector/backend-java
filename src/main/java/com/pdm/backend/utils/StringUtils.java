package com.pdm.backend.utils;

import java.util.List;

/**
 * @Author: baojing.he
 * @Date: 2021-01-20 14:54
 * @Description:
 */
public class StringUtils {

    /**
     * 集合是否为空
     *
     * @param list
     * @return
     */
    public static boolean isListEmpty(List list) {
        if (list == null || list.size() == 0) {
            return true;
        }
        return false;
    }

    /**
     * 字符串是否为空
     *
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        if (str == null || str.trim().length() == 0) {
            return true;
        }
        return false;
    }

}
