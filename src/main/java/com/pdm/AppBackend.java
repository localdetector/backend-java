package com.pdm;

/*
 * @author  baojing.he
 * @date  2022-02-12 3:35 PM
 */

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class AppBackend {
    public static void main(String[] args) {

        SpringApplication.run(AppBackend.class, args);
        log.info("Backend-java starts up successfully");
    }

}


